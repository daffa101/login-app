import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

Vue.config.productionTip = false;

let app = '';

const config = {
  apiKey: "AIzaSyAF720Bnx56rZsuz6QXL4nhAO4UOqqCaHI",
  authDomain: "loginapp-344f4.firebaseapp.com",
  databaseURL: "https://loginapp-344f4.firebaseio.com",
  projectId: "loginapp-344f4",
  storageBucket: "",
  messagingSenderId: "345860072756",
  appId: "1:345860072756:web:a610d30f07fc001abf7161"
};

firebase.initializeApp(config);

export const db = firebase.firestore();

firebase.auth().onAuthStateChanged(() => {
  if(!app){
    new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
})